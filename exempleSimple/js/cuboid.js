import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";
let cuboid = reactive({
  length: 2,
  width : 3,
  height: 4,
  incrLength: function (){
    this.length++;
  },
  decrLength: function (){
    this.length--;
  },
  incrWidth: function (){
    this.width++;
  },
  decrWidth: function (){
    this.width--;
  },
  incrHeight: function (){
    this.height++;
  },
  decrHeight: function (){
    this.height--;
  },
  volume: function () {
    return this.length * this.width * this.height;
  },
  monStyle: function() {
    return {width:'100px',height:'340px'};
  },
  dimensionDuRectangle: function() {
    return {width:(34*this.width)+'px',height:(34*this.height)+'px'};
  }
}, "cuboidHTML");

startReactiveDom();

