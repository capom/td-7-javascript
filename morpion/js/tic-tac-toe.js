import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";

let game = reactive({board:Array(9).fill(""), nextPlayer: "O", winner:undefined, roundNumber:1}, "game");
let previousState = [{},{},{},{},{},{},{},{},{}];


game.getboard = function(i){
  return game.board[i];
}

game.isWinner = function(P){
  const winLines = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
  for(let line of winLines){
    if(game.board[line[0]] === P && game.board[line[1]] === P && game.board[line[2]] === P)
      return true;
  }
  return false;
}

game.play = function(i){
  if(game.getboard(i)!=='')return;
  Object.assign(previousState[this.roundNumber-1],this)
  let newBoard = [...game.board];
  newBoard[i] = game.nextPlayer;
  game.board = newBoard;
  game.nextPlayer = game.nextPlayer==="O"?"X":"O"
  game.roundNumber++;
}

game.restart = function(){
  Object.assign(game,{board:Array(9).fill(""), nextPlayer: "O", winner:undefined, roundNumber:1})
}

game.setWinner= function(){
  if(game.isWinner("O")){
    game.winner="O";
  }else if(game.isWinner("X")){
    game.winner="X";
  }
  else {
    game.winner=undefined
  }
}

game.round = function(i){
  Object.assign(this,previousState[i])
}
game.backRoundButtons = function (){
  let i = game.roundNumber;
  let boutons=''
  for (let j = 0; j < i-1; j++) {
    boutons+=`<button data-onclick="game.round(${j})">${j+1}</button>`
  }
  return boutons;
}

applyAndRegister(()=>{game.setWinner()})

startReactiveDom();
