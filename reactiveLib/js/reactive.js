let objectByName = new Map();
let registeredEffects = new Set();

function applyAndRegister(effect){
  effect();
  registeredEffects.add(effect)
}
function trigger() {
  for (const effect of registeredEffects) {
    effect();
  }
}
window.t=trigger
function reactive(passiveObject, name){
  const handler = {
    set(target, key, value) {
      target[key] = value;
      trigger();
      return true;
    },
  };
  let reactiveObjet = new Proxy(passiveObject, handler);

  objectByName.set(name, reactiveObjet);
  return reactiveObjet;
}

function startReactiveDom(){
  for (let elementClickable of document.querySelectorAll("[data-onclick]")){
    const [nomObjet, methode, argument] = elementClickable.dataset.onclick.split(/[.()]+/);
    elementClickable.addEventListener('click', (event) => {
      const objet = objectByName.get(nomObjet);
      objet[methode](argument);
    })
  }
  for (let element of document.querySelectorAll("[data-textfun]")){
    const [nomObjet, methode, argument] = element.dataset.textfun.split(/[.()]+/);
    let objet = objectByName.get(nomObjet);
    element.textContent=objet[methode]();
  }

  for (let rel of document.querySelectorAll("[data-textfun]")){
    const [obj, fun, arg] = rel.dataset.textfun.split(/[.()]+/);
    applyAndRegister(()=>{rel.textContent = objectByName.get(obj)[fun](arg)});
  }
  for (let rel of document.querySelectorAll("[data-textvar]")){
    const [obj, prop] = rel.dataset.textvar.split('.');
    applyAndRegister(()=>{rel.textContent = objectByName.get(obj)[prop]});
  }
}


export {applyAndRegister, reactive, startReactiveDom};